poweredFuncs = map (^) [2 ..]

res n = map ($ n) poweredFuncs

logged = map log $ res 5