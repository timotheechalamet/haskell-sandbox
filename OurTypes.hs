module OurTypes (Coords (..), toCart, distance) where

data Coords = Cart Double Double | Polar Double Double deriving (Show)

toCart :: Coords -> Coords
toCart (Polar r t) = Cart (r * cos t) (r * sin t)
toCart (Cart x y) = Cart x y

distance :: Coords -> Coords -> Double
distance p q = eucludian (toCart p) (toCart q)
  where
    eucludian (Cart x1 y1) (Cart x2 y2) = sqrt $ (x2 - x1) ^ 2 + (y2 - y1) ^ 2
